<?php

/**
 * @file
 * Admin pages file for the Webform redmine module.
 */

/**
 * Form builder to configure webform_redmine module.
 *
 * @return array
 *   Array containing renderable form for webform_redmine Redmine connection
 *   settings page.
 */
function webform_redmine_admin_rmapi_config($form, &$form_state) {
  $form['webform_redmine_rmurl'] = array(
    '#type' => 'textfield',
    '#title' => t('Redmine URL'),
    '#description' => t('The complete URL for Redmine.'),
    '#default_value' => variable_get('webform_redmine_rmurl', 'http://example.redmine.com'),
    '#required' => TRUE,
  );
  $form['webform_redmine_rmapikey'] = array(
    '#type' => 'textfield',
    '#title' => t('Redmine API access key'),
    '#description' => t("Redmine API access key for a authorized user.  Located under \'My account\' in your Redmine installation."),
    '#default_value' => variable_get('webform_redmine_rmapikey', 'Paste your API access key here.'),
    '#required' => TRUE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Next'),
  );
  return $form;
}

/**
 * Validate user input from webform_redmine_admin_rmapi_config.
 */
function webform_redmine_admin_rmapi_config_validate($form, &$form_state) {
  $webform_redmine_rmurl = $form_state['values']['webform_redmine_rmurl'];
  $webform_redmine_rmapikey = $form_state['values']['webform_redmine_rmapikey'];

  if (!filter_var($webform_redmine_rmurl, FILTER_VALIDATE_URL)) {
    form_set_error('webform_redmine_rmurl', t('Please enter a valid URL. i.e. http://example.redmine.com'));
  };

  if (strlen($webform_redmine_rmapikey) < 40) {
    form_set_error('webform_redmine_rmapikey', t('Please enter a valid Redmine API access key. (Min. 40 char.)'));
  };
}

/**
 * Form submit handler for webform_redmine_admin_rmapi_config.
 */
function webform_redmine_admin_rmapi_config_submit($form, &$form_state) {
  variable_set('webform_redmine_rmurl', $form_state['values']['webform_redmine_rmurl']);
  variable_set('webform_redmine_rmapikey', $form_state['values']['webform_redmine_rmapikey']);
  $form_state['redirect'] = 'admin/config/services/webform_redmine_settings';
}

/**
 * Form builder to select Redmine project.
 *
 * @return array
 *   Array containing renderable form for selecting a Redmine project to
 *   use with webform_redmine.
 */
function webform_redmine_rmproject_select($form, &$form_state) {
  // Verify Redmine API settings have been configured.
  $rmkey_check = variable_get('webform_redmine_rmapikey', NULL);
  if (!isset($rmkey_check)) {
    $form['notice'] = array(
      '#type' => 'item',
      '#title' => t('NOTICE'),
      '#markup' => t('Configure your !apisettings first.', array('!apisettings' => l(t('Redmine API settings'), 'admin/config/services/webform_redmine'))),
    );
    return $form;
  }
  else {
    // Retrieve and set the Redmine UID to set as default asignee.
    $rmuid = webform_redmine_rmuid();
    // Verify a UID exists.
    if ($rmuid) {
      variable_set('webform_redmine_rmuid', $rmuid);
    }
    else {
      // Notify user before proceeding.
      drupal_set_message(t('Unable to assign a Redmine UID.'), 'warning');
    }
    // Retrieve list of available projects from Redmine.
    $projects = webform_redmine_getrmprojects();
  }

  // Verify there are projects to select from.
  if (empty($projects)) {
    $args = array(
      '@apisettings' => l(t('Redmine API settings'), 'admin/config/services/webform_redmine'),
      '@recentlogs' => l(t('recent log entries'), 'admin/reports/dblog'),
    );
    $form['notice'] = array(
      '#type' => 'item',
      '#title' => t('Unable to retireve a list of projects from Redmine:'),
      '#markup' => t('Suggestions:<ul><li>Verify your @apisettings.</li><li>Check the @recentlogs for additional details.</li></ul>', $args),
    );
    return $form;
  }
  else {
    // Return a project selection form.
    $form['webform_redmine_rmprojectid'] = array(
      '#type' => 'radios',
      '#title' => t('Select a Redmine project to post feedback issues.'),
      '#options' => $projects,
      '#required' => TRUE,
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Finish'),
    );
    return $form;
  }
}

/**
 * Form submit handler for webform_redmine_rmproject_select.
 */
function webform_redmine_rmproject_select_submit($form, &$form_state) {
  variable_set('webform_redmine_rmprojectid', $form_state['values']['webform_redmine_rmprojectid']);
  $form_state['redirect'] = 'node';
}

/**
 * Retrieves the Redmine user information for the currently configured.
 *
 * API key and returns the Redmine UID.
 *
 * @return int
 *   Redmine user ID based on API key.
 */
function webform_redmine_rmuid() {
  // Retrieve Redmine API settings.
  $rm_api_key = variable_get('webform_redmine_rmapikey', NULL);
  $rm_base_url = variable_get('webform_redmine_rmurl', NULL);

  // Verify settings.
  if (!isset($rm_api_key)) {
    $msg = t('Unable to retrieve Redmine project list because no API key is present.');
    watchdog('webform_redmine', $msg);
    return 0;
  }

  // Prepare request.
  $options = array(
    'headers' => array(
      'Content-Type' => 'application/json',
      'X-Redmine-API-Key' => $rm_api_key,
    ),
    'method' => 'GET',
  );
  $rm_endpoint = '/users/current.json';
  $request_url = $rm_base_url . $rm_endpoint;

  // Get response.
  $response = drupal_http_request($request_url, $options);

  // Verify response and retrieve results.
  if ($response->status_message == 'OK') {
    $data = json_decode($response->data);
    return $data->user->id;
  }
  else {
    $msg = t('Unable to retreve user ID from Redmine.<br><strong>Response:</strong><br><code>@response</code>');
    $vars = array('@response' => json_encode($response));
    watchdog('webform_redmine', $msg, $vars);
  }
}

/**
 * Retrieve a list of projects from the specified Redmine instance.
 *
 * @return array
 *   Returns a key / value pair array for projects currently available
 *   within Redmine with project id as key and name as value.
 */
function webform_redmine_getrmprojects() {
  // Retrieve Redmine API settings.
  $rm_api_key = variable_get('webform_redmine_rmapikey', NULL);
  $rm_base_url = variable_get('webform_redmine_rmurl', NULL);

  // Verify settings.
  if (!isset($rm_api_key)) {
    $msg = t('Unable to retrieve Redmine project list because no API key is present.');
    watchdog('webform_redmine', $msg);
    return array();
  }

  // Prepare request.
  $options = array(
    'headers' => array(
      'Content-Type' => 'application/json',
      'X-Redmine-API-Key' => $rm_api_key,
    ),
    'method' => 'GET',
  );
  $rm_endpoint = '/projects.json';
  $rm_options = array(
    'limit' => '100',
  );
  $request_url = $rm_base_url . $rm_endpoint . '?' . drupal_http_build_query($rm_options);

  // Get response.
  $response = drupal_http_request($request_url, $options);

  // Verify response and retrieve results.
  if ($response->status_message == 'OK') {
    $results = json_decode($response->data);
    // Verify atleast a single project was retrieved.
    if (count($results->projects) < 1) {
      return array();
    }
  }
  else {
    $msg = t('Recieved a bad response while attempting to retrieve Redmine projects.<br><strong>Response as JSON:</strong><br><code>@response</code>');
    $vars = array('@response' => json_encode($response));
    watchdog('webform_redmine', $msg, $vars);
    return array();
  }

  // Parse results and return list of project names keyed by project id.
  foreach ($results->projects as $project) {
    $rm_projects[$project->id] = $project->name;
  };
  return $rm_projects;
}
