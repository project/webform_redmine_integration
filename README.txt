Webform Redmine Integration.

This module can post the users inputs from a Webform to Redmine Backend.
Currently only Redmine tickets can be created.
Further Integrations will be looked into once the code is made more generic.

The intention was to get it going and make it work with Webforms as
Webforms are the most used survey tools in Drupal.

To configure this module, simply enable it and
go to admin/config/services/webform-redmine.
Enter your Redmine credentials.

Once you enter your credentials node/%nodeid/webform/webform-redmine-mapping

Module was developed while I was working at
<a href="https://www.drupal.org/marketplace/azri">Azri</a>.
